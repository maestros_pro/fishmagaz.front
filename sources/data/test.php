<?php
header('Access-Control-Allow-Origin: *');
sleep(1);

$data = $_POST;


$jsonFalse = '{
	"status": false,
	"title": "Ошибка",
	"message": "Неверные логин и пароль."
}';


if ($data['passrecovery']) {
	$json = '{
		"status": true,
		"title": "восстановление пароля",
		"message": "Инструкции по восстановлению пароля были<br>направлены на адрес <span>' . $data['passrecovery'] . '</span>"
	}';

} else if ($data['regEmail'] && $data['regPass'] && $data['regPassRepeat']) {
	$json = '{
		"status": true,
		"title": "подтверждение регистрации",
		"message": "Вы успешно прошли регистрацию на нашем сайте,<br>на указанную почту отправлено письмо для<br>подтверждения регистрации.",
		"button": "Войти",
		"redirectURL": "/login.html#auth"
	}';

} else {
	$json = $jsonFalse;
}

echo $json;
?>