String.prototype.discharge = function (mark) {
	var x = this.split('.'),
		x1 = x[0],
		x2 = x.length > 1 ? '.' + x[1] : '',
		rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + (mark ? mark : ' ') + '$2');
	}
	return x1 + x2;
};
Number.prototype.discharge = function (mark) {
	var val = this + '';
	return val.discharge();
};







(function(){
	var app = {
		module: {

			iosFix: {

				removeIOSRubberEffect: function(el) {

					var $body = $('body'),
						t,
						st,
						wh,
						h,
						y0 = 0,
						y1 = 0,
						yd = 0;

					$body
						.on('touchstart', el, function (e) {
							var $t = $(this);

							y0 = e.pageY || e.originalEvent.targetTouches[0].pageY;

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);
						})
						.on('touchmove', el, function (e) {
							var $t = $(this);

							if ( y0 !== 0){
								y1 = e.pageY || e.originalEvent.targetTouches[0].pageY;
								yd = (( y1 - y0 ) > 0 ) ? (-1) : ( ( y1 - y0 ) < 0 ? (1) : 0);
							}

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);

							if ( st <= 0 && yd < 0){
								$t.scrollTop(st);
								e.preventDefault();
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else if ( st >= (wh - h) && yd > 0){
								$t.scrollTop(wh - h);
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else {
								$body.off('touchmove.popupbg');
							}

						})
						.on('touchend', el, function (e) {
							$body.off('touchmove.popupbg');
						});
				},

				init: function(){

					if ( !!Modernizr ){

						Modernizr.addTest('ipad', function () {
							return !!navigator.userAgent.match(/iPad/i);
						}).addTest('iphone', function () {
							return !!navigator.userAgent.match(/iPhone/i);
						}).addTest('ipod', function () {
							return !!navigator.userAgent.match(/iPod/i);
						}).addTest('ios', function () {
							return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
						});
					}

					if ( Modernizr.ipad || Modernizr.ipod || Modernizr.iphone ){

						self.iosFix.removeIOSRubberEffect('.nav');
						self.iosFix.removeIOSRubberEffect('.popup');

					}

				}
			},

			setMenuPosition: {

				pos: function (el) {
					var $el = $(el);
					$el.scrollToFixed({
						marginTop: 10,
						limit: $('.footer').offset().top - $el.outerHeight() - 10,
						zIndex: 999,
						removeOffsets: true
					});
				},

				init: function () {
					var $el = $('.cart__preview-inner');
					self.setMenuPosition.pos($el);
					$(window).resize(function () {
						$el.trigger('detach.ScrollToFixed');
						self.setMenuPosition.pos($el);
					});
				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form__field'),
						val = $.trim($t.val()),
						errMes = $t.data('error-message'),
						rexp = /.+$/igm;

					$wrap.removeClass('error').removeClass('success');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || !val ) val = false;
					} else if ( $t.attr('type') == 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								break;
							//case 'phone':
							//	rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
							//	break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								break;
							case 'password':
								//rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
								rexp = /[a-zA-Z0-9_-]{6}/;
								break;
							default:
								rexp = /.+$/igm;
						}
					}


					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if ( errMes ){
							$wrap.find('.form-message').html(errMes).slideDown(200);
						}

					} else {
						$wrap.addClass('success');
						if ( errMes ){
							$wrap.find('.form-message').slideUp(200);
						}
					}

				});


				return !wrong;

			},

			tooltip: {
				init: function () {
					var $tooltip = $('.tooltip');

					if (!$tooltip.size()) {
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('.wrapper');
					}

					$('body').on('mouseover', '[data-tooltip]', function () {
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html('<span class="tooltip-inner">' + text + '</span>').stop(true, true).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth() / 2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function () {
						$tooltip.removeClass('show');
					});


				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup-inner">'
						+				'<div class="popup-layout">'
						+					'<div class="popup-close"></div>'
						+					'<div class="popup-content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup-overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup-content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback){

					var title = mes.title ? '<div class="popup-title">' + mes.title + '</div>' : '',
						text = mes.message ? '<div class="popup-text">' + mes.message + '</div>' : '',
						btnText = mes['button'] ? mes['button'] : 'Ок';

					var html = title + text
						+ '<div class="popup-link"><span class="btn">' + btnText + '</span></div>';
					self.popup.open('.popup_info', html);

					$('.popup_info').find('.btn').click(function(){
						self.popup.close($('.popup_info'));
						if ( callback ) callback();
					});
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				}
			},

		},
		init: function() {

			self.tooltip.init();

			self.setMenuPosition.init();

			var $b = $('body');

			$('[data-required=phone]').mask('+7 (999) 999-99-99');

			$('.card__list_slider').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 5,
				swipe: false,
				swipeToSlide: false,
				touchMove: true,
				slidesToScroll: 1,
				prevArrow: '<div class="slick-arrow slick-prev"></div>',
				nextArrow: '<div class="slick-arrow slick-next"></div>',
				responsive: [
					{
						breakpoint: 1620,
						settings: {
							slidesToShow: 4
						}
					}
				]
			});


			$('.preview_slider').slick({
				dots: false,
				infinite: false,
				speed: 300,
				slidesToShow: 3,
				swipe: false,
				swipeToSlide: false,
				touchMove: true,
				slidesToScroll: 1,
				prevArrow: '<div class="slick-arrow slick-prev"><svg width="18" height="11" viewBox="0 0 18 11"><path d="M17.989,8.8L15.738,11,8.987,4.392,2.235,11l-2.25-2.2,9-8.81Z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-next"><svg width="18" height="11" viewBox="0 0 18 11"><path d="M17.989,2.2L15.738,0,8.987,6.608,2.235,0l-2.25,2.2,9,8.81Z"/></svg></div>',
				vertical: true,
				responsive: [
					{
						breakpoint: 1620,
						settings: {
							slidesToShow: 3,
							prevArrow: '<div class="slick-arrow slick-prev"><svg width="11" height="18" viewBox="0 0 11 18"><path class="cls-1" d="M11,15.765l-2.2,2.25-8.81-9,8.81-9L11,2.262,4.392,9.013Z"/></svg></div>',
							nextArrow: '<div class="slick-arrow slick-next"><svg width="11" height="18" viewBox="0 0 11 18"><path class="cls-1" d="M0,15.765l2.2,2.25,8.81-9-8.81-9L0,2.262,6.608,9.013Z"/></svg></div>',
							vertical: false
						}
					}
				]
			});


			$('.js-datepicker').fdatepicker({
				//initialDate: '02-12-1989',
				format: 'dd.mm.yyyy',
				language: 'ru',
				disableDblClickSelection: true,
				weekStart: 1,
				leftArrow: '<',
				rightArrow: '>',
				//closeIcon:'X',
				//closeButton: true
				startDate: new Date()
			});


			$b
				.on('click', '.scrollto', function(e){
					e.preventDefault();
					var $this = $(this.hash);
					$('html,body').animate({scrollTop:$this.offset().top}, 500);
				})
				.on('click', '.delivery__showmore', function (e) {
					e.preventDefault();

					var $t = $(this),
						$wrap = $t.closest('.delivery__item'),
						$more = $wrap.find('.delivery__more');

					$more.slideToggle(300, function () {
						$wrap.toggleClass('is-open');
					});
				})
				.on('click', '.preview__link', function (e) {
					e.preventDefault();

					var $t = $(this),
						$wrap = $t.closest('.product__pic'),
						$img = $wrap.find('.product__pic-main').find('img');

					if (!$wrap.hasClass('is-loading')) {
						$wrap.find('.preview__link').removeClass('is-active');
						$t.addClass('is-active');
						$wrap.addClass('is-loading');
						$img.fadeOut(200);
						var img = new Image();
						img.src = $t.attr('href');
						img.onload = function () {
							$img.attr('src', img.src).fadeIn(200);
							$wrap.removeClass('is-loading');
						};
					}
				})
				.on('click', '.faq__item-q', function (e) {
					e.preventDefault();

					var $t = $(this),
						$wrap = $t.closest('.faq__item'),
						$more = $wrap.find('.faq__item-a');

					$more.slideToggle(300, function () {
						$wrap.toggleClass('is-open');
					});
				})

				.on('keyup blur', '.js-address-map-input', function (e) {
					e.preventDefault();

					if ($.trim($(this).val())) {
						$('.js-address-map-btn').prop('disabled', false);
					} else {
						$('.js-address-map-btn').prop('disabled', true);
					}

				})

				.on('keyup blur change', '.js-input-search', function (e) {
					e.preventDefault();
					var $t = $(this);

					if ($.trim($t.val())) {
						$t.addClass('is-filled');
					} else {
						$t.removeClass('is-filled');
					}

				})

				.on('click', '.js-search-result-item', function (e) {
					e.preventDefault();
					var $t = $(this), $wrap = $t.closest('.header__search'), $input = $wrap.find('.js-input-search');

					$input.val($t.data('value')).trigger('change');
					$wrap.find('.result').remove();


				})

				.on('click', '.js-product-order-sum', function (e) {
					e.preventDefault();
					var $t = $(this),
						$wrap = $t.closest('.order-sum'),
						$input = $wrap.find('.order-sum__val'),
						step = Number($wrap.data('step')),
						val = Number($input.text());

					if ($t.hasClass('order-sum__less')) {
						val -= step;
					} else if ($t.hasClass('order-sum__more')) {
						val += step;
					}

					if (val < step) val = step;

					$input.text(+val.toFixed(10));

				})

				.on('keydown', '.js-timepicker', function (e) {
					e.preventDefault();
					return false;
				})

				.on('mousedown', '.timepicker__item', function (e) {
					e.preventDefault();
					var $t = $(this), $input = $t.closest('.form__input').find('.js-timepicker');

					if ($t.hasClass('is-disable')) {
						$input.blur();
						return false;
					}
					$input.val($t.text()).blur();
				})




				.on('click', '[data-popup]', function(e){
					e.preventDefault();
					e.stopPropagation();
					self.popup.close('.popup');
					self.popup.open($(this).data('popup'));
				})
				// popup close
				/*.on('click', '.popup', function(e){
					if ( !$(e.target).closest('.popup-layout').size() ) self.popup.close('.popup');
				 })*/
				.on('click', '.popup-close, .js-popup-close', function (e) {
					e.preventDefault();
					var $t = $(this),
						$info = $t.closest('.popup_info');
					if ($info.size() && $info.find('.btn').size()) $info.find('.btn').click();
					self.popup.close('.popup');
				})
				// tabs
				.on('click', '[data-tab-link]', function(e){

					e.preventDefault();
					var $t = $(this),
						group = $t.data('tab-group'),
						$links = $('[data-tab-link]').filter(selectGroup),
						$tabs = $('[data-tab-targ]').filter(selectGroup),
						ind = $t.data('tab-link'),
						$tabItem = $('[data-tab-targ=' + ind + ']').filter(selectGroup),
						time = 150;

					if (!$t.hasClass('is-active')) {
						$links.removeClass('is-active');
						$t.addClass('is-active');
						$tabs.removeClass('is-active');
						$tabItem.addClass('is-active');


						loginHandler();
					}

					function selectGroup(){
						return $(this).data('tab-group') === group;
					}

				})

				.on('submit', '.js-form-ajax', function (e) {
					e.preventDefault();
					e.stopPropagation();
					var $form = $(this),
						validate = self.checkForm($form),
						type = $form.data('type'),
						callback = function () {
						};

					if (!validate || $form.hasClass('is-loading')) {
						if ($('.form__field.error').eq(0).offset().top < $(window).scrollTop()) $('html,body').animate({scrollTop: $('.form__field.error').eq(0).offset().top - 50}, 500);
						return false;
					}
					$form.addClass('is-loading');

					$.ajax({
						url: $form.attr('action'),
						method: $form.attr('method'),
						data: $form.serialize(),
						dataType: 'json'
					}).success(function (data) {

					}).complete(function (xhr, textStatus) {
						var res = {};
						try {
							res = JSON.parse(xhr.responseText);
						} catch (e) {
						}
						if (res.status == true) {

							switch (type) {
								case 'auth':
									callback = function () {
										window.location = location.origin;
									};
									break;
								case 'reg':
									callback = function () {
										window.location = location.origin;
									};
									break;
								case 'recovery':
									callback = function () {
										window.location = location.origin;
									};
									break;
								default:

							}
						}
						if (res.redirectURL) {
							callback = function () {
								window.location.reload(res.redirectURL);
							};
						}
						if (res.message || res.title) {
							self.popup.info(res, callback);
						}
						$form.removeClass('is-loading');
					});


				})

			;

			$('.filter-range').each(function () {
				var $t = $(this),
					slider = $t[0],
					rangeStep = 30;

				//$t.find('.filter-range__min').text($t.data('range-min'));
				//$t.find('.filter-range__max').text($t.data('range-max'));

				noUiSlider.create(slider, {
					start: [$t.data('val-min'), $t.data('val-max')],
					margin: 1,
					step: 1,
					animate: true,
					connect: true,
					//step: rangeStep,
					//step: rangeStep,
					range: {
						'min': $t.data('range-min'),
						'max': $t.data('range-max')
					}
				});

				slider.noUiSlider.on('change', function (values, handle) {
					setValue($t, values);
				});

				slider.noUiSlider.on('slide', function (values, handle) {
					setValue($t, values);
				});

				setValue($t, [$t.data('val-min'), $t.data('val-max')]);

			});

			function setValue($slider, val) {
				for (var i = 0; i < val.length; i++) {
					$slider.find('.noUi-handle[data-handle=' + i + ']').html('<span>' + Math.round(val[i]).discharge() + '<span>');
				}
				$slider.find('.i-from').val(Math.round(val[0]));
				$slider.find('.i-to').val(Math.round(val[1]));
			}




			if (location.hash) {
				$('[data-tab-link=' + location.hash.replace('#', '') + ']').click();
			}


			loginHandler();
			$(window).resize(loginHandler);

			function loginHandler() {
				var $handler = $('.line-handler'),
					$marker = $('.js-frame');

				if ($handler.size()) {
					var $active = $marker.filter(function () {
							return $(this).closest('.is-active').size();
						}),
						width = $active.eq($active.length - 1).position().left + $active.eq($active.length - 1).width() - $active.eq(0).position().left,
						left = $active.eq(0).position().left;

					$handler.css({'left': left, 'width': width});
				}

			}

		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();

